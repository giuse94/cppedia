"use strict";
/* Called in login.php when submitting the form */
function checkLogin() {
  var userOk, pswOk;
  // Get the value attribute of the input field, not its innerHTML property, which is always empty
  var username = document.getElementById("username").value.trim();
  // Get the span element that contains the error message if the input field is empty.
  // You can't get its innerHTML property, assign it to a variable and then assign a new value to the
  // variable, because it wouldn't update the content of the element (you would need a reference).
  var userSpan = document.getElementById("userEmpty");
  if (username == "") {
    userSpan.innerHTML = "* Inserisci il tuo username";
    userOk = false; // Empty field, can't submit
  }
  else {
    userSpan.innerHTML = "";
    userOk = true;
  }
  var psw = document.getElementById("password").value; // Don't trim the password
  var pswSpan = document.getElementById("pswEmpty");
  if (psw == "") {
    pswSpan.innerHTML = "* Inserisci la tua password";
    pswOk = false;
  }
  else {
    pswSpan.innerHTML = "";
    pswOk = true;
  }
  return userOk && pswOk; // Can submit if both input fields are filled
}

/* Called in registra.php when submitting the form */
function checkRegister() {
  var userOk, pswOk, chkPswOk;
  var username = document.getElementById("username").value.trim();
  var userSpan = document.getElementById("userEmpty");
  if (username == "") {
    userSpan.innerHTML = "* Inserisci il tuo username";
    userOk = false;
  }
  else {
    userSpan.innerHTML = "";
    userOk = true;
  }
  var psw = document.getElementById("password").value;
  var pswSpan = document.getElementById("pswEmpty");
  if (psw == "") {
    pswSpan.innerHTML = "* Inserisci la tua password";
    pswOk = false;
  }
  else { // Check the last input (confirm password) only if the user entered a password
    pswSpan.innerHTML = "";
    pswOk = true;
    var chkPsw = document.getElementById("chkPsw").value;
    var chkPswSpan = document.getElementById("chkPswEmpty");
    if (chkPsw != psw) {
      chkPswSpan.innerHTML = "* Le password non combaciano";
      chkPswOk = false;
    }
    else {
      chkPswSpan.innerHTML = "";
      chkPswOk = true;
    }
  }
  return userOk && pswOk && chkPswOk;
}

/* Called in modifica.php when submitting the form */
function checkEdit() {
  var descrOk, txtOk;
  var descrizSpan = document.getElementById("descrizEmpty");
  if (document.getElementById("descriz").value.trim() == "") {
    descrizSpan.innerHTML = "* Campo obbligatorio";
    descrOk = false;
  }
  else {
    descrizSpan.innerHTML = "";
    descrOk = true;
  }
  
  var textarea = document.getElementById("txtArea");
  var wrongTxtSpan = document.getElementById("wrongTxtArea");
  // Find "<" not followed by "p>" or "a>" etc. "\x2F" is "/" for closing tags.
  // A match is then a forbidden tag
  var regex = /<(?!p>|a>|h[1-6]>|em>|code>|\x2F)/gi; // This way you can't use attributes
  if (regex.test(textarea.value)) {
    wrongTxtSpan.innerHTML = "* Sono stati inseriti tag non consentiti";
    wrongTxtSpan.scrollIntoView();
    window.scrollBy(0, -50); // The span el. is not visible due to the fixed topNavBar: scroll a bit up
    txtOk = false;
  }
  else {
    wrongTxtSpan.innerHTML = "";
    txtOk = true;
  }
  return descrOk && txtOk;
}

/* Called in modifica.php when you click on a link (onclick event) */
function confirmExit(event) {
  event.preventDefault(); // Cancel the default action associated with the "click" event
  event.returnValue = ''; // Chrome requires returnValue to be set
  var aEl = document.getElementById('abbandona'); // Get the <a> element "Abbandona la pagina"
  aEl.removeEventListener('click', confirmExit); // Remove the event handler previously attached
  window.removeEventListener('beforeunload', myfun);
  aEl.href = this.href; // "This" refers to the <a> element that was clicked
  document.getElementById('modal').style.display='block'; // Show modal box
}

function cleanTextArea(txt) {
  /* Cerca < non seguito da h2|p|... e lo sostituisce con &lt; */
  var regex = /<(?!h2|p|a|code|pre|\x2F)/gi; // \x2F è /
  return txt.replace(regex, "&lt;");
}

function validateEditForm() {

}

/* Called in snackbar.php when there is a notification to be shown */
function showSnackbar() {
  var x = document.getElementById("snackbar");
  x.className = "show"; // Add the "show" class to the snackbar element
  // Remove the show class from after 5 seconds
  setTimeout(function() { x.className = x.className.replace("show", ""); }, 5000);
}

 /* Called in errore.php when you press the button labeled "Mostra/Nascondi dettagli" */
function showHideErrorInfoPanel(btn) {
  var panel = document.getElementById("errorInfoPanel");
  if (panel.className.indexOf("w3-show") == -1) { // Panel not visible
    panel.className += " w3-show"; // Show panel
    btn.innerHTML = '<i class="fas fa-minus"></i> Nascondi dettagli'; // Change button content
  }
  else {
    panel.className = panel.className.replace(" w3-show", "");
    btn.innerHTML = '<i class="fas fa-plus"></i> Mostra dettagli';
  }
}
