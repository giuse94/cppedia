﻿<?php
$pageTitle = 'Esci';
$editable = false;
$hasCrono = false;
$topBarActiveLink = "Esci";

$prevPage = empty($_GET['returnTo']) ? "leggi.php?page=Home" : $_GET['returnTo'];

require 'required/files.php';

if (isset($_SESSION['username'])) {  
  unset($_SESSION['username']);
  $_SESSION['notification'] = "Logout effettuato";
  header("Location:$prevPage");
  exit(); // Just for
}
echo "    <p>Uscita già eseguita.</p>\n";
require 'required/footer.php';
?>