<?php
$pageTitle = "Cerca";
$topBarActiveLink = "Cerca";
$editable = false;
$hasCrono = false;

$searchPage = empty($_GET['page']) ? "" : $_GET['page'];

require 'required/files.php';

$query = "SELECT COUNT(*) AS n FROM pagina WHERE nome = '$searchPage';";
if ( ! ($result = mysqli_query($conn, $query) ) )
  handleError("DB query error: " . mysqli_error($conn));
if (mysqli_fetch_assoc($result)['n'] == 1) {
  header("Location:leggi.php?page=$searchPage");
  exit(); // Just for
}

if (empty($searchPage)) {
  $h1Txt = "Cerca";
  $pTxt = "Cerca una voce su <em>Cppedia</em>.";
}
else {
  $h1Txt = "Risultati della ricerca";
  $pTxt = "Su <em>Cppedia</em> non esiste la voce «" . $searchPage . "» e al momento non è possibile crearla.";
}
?>
    <h1><?=$h1Txt?></h1>
    <p><?=$pTxt?></p>
    <form class="w3-section">
      <label>Pagina:</label>
      <input type="text" class="w3-margin-bottom" name="page" value="<?=$searchPage?>" /><br />
      <button type="submit" class="w3-button w3-theme w3-round-large">
        <i class="fa fa-search"></i> Cerca
      </button>
    </form>
<?php
require 'required/snackbar.php';
require 'required/footer.php';
?>