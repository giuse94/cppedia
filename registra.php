<?php
$pageTitle = 'Registra account';
$editable = false;
$hasCrono = false;
$topBarActiveLink = "Registrati";

$prevPage = empty($_GET['returnTo']) ? "leggi.php?page=Home" : $_GET['returnTo'];

require 'required/files.php';

$username = $password = $pswCheck = "";
$userEmpty = $pswEmpty = $pswChkEmpty = "";

function sanitizeInput($text) {
  $text = trim($text);
  $text = stripslashes($text);
  $text = htmlentities($text, ENT_QUOTES);
  $text = mysql_real_escape_string($text);
  return $text;
}

if (isset($_SESSION['username'])) {
  echo "    <h1>Crea un nuovo account</h1>\n";
  echo "    <p>Hai già effettuato l'accesso con l'utenza " . $_SESSION['username'] .
       ": non puoi crearne un'altra.</p>\n";
  require 'required/footer.php';
  exit();
}

if ($_SERVER["REQUEST_METHOD"] == "POST") { // User submitted data by clicking on "Registra account"
  if (isset($_POST['username']))
    $username = sanitizeInput($_POST['username']);
  if (empty($username))
    $userEmpty = "* Campo obbligatorio";
  if (empty($_POST['password'])) // empty($var) returns true if $var is not set, i.e. it does not exist
    $pswEmpty = "* Campo obbligatorio";
  else { // Needless to check $pswchk if there's no password
    $password = $_POST['password'];
    if (empty($_POST['pswchk']))
      $pswChkEmpty = "* Campo obbligatorio";
    else if ($_POST['pswchk'] != $password)
      $pswChkEmpty = "* Le password non coincidono";

    // Check if the chosen username already exists
    $query = "SELECT COUNT(*) AS numUsr FROM utente WHERE username='$username';";
    // $conn = connect() (to database) is defined in leftSideBar.php
    if ( ! ( $result = mysqli_query($conn, $query) ) )
      handleError("DB query error: " . mysqli_error($conn));
    if (mysqli_fetch_assoc($result)['numUsr'] >= 1) // Should always be 1
      $userEmpty = "* Username già utilizzato: scegline un altro";
  }
  // If fields are filled and passwords match, create the new account
  if (!$userEmpty && !$pswEmpty && !$pswChkEmpty) {
    $password = md5($password);
    $dataReg = date("Y-m-d"); // Current date in YYYY-MM-DD format
    $oraReg = date("H:i:s"); // Current time in HH:MM:SS format

    $query = "INSERT INTO utente (username, password, dataRegistrazione, oraRegistrazione)
                          VALUES ('$username', '$password', '$dataReg', '$oraReg');";

    if ( ! mysqli_query($conn, $query) )
    handleError("DB query error: " . mysqli_error($conn));

    $_SESSION['username'] = $username;
    $_SESSION['notification'] = "L'utenza <em>$username</em> è stata creata"; // Used in snackbar.php which will be included by $prevPage
    header("Location:$prevPage");
    exit(); // Just for
  } // End of if (canRegister)
} // End of if (method == POST)
?>
    <h1>Crea un nuovo account</h1>
    <p>
      Scegli un username e una password. Questi saranno i dati che ti serviranno per effettuare il login su Cppedia e
      modificare le pagine. Clicca su "Registra account" per confermare.
    </p>
    <form method="post" onsubmit="return checkRegister()" style="margin-left:auto; margin-right:auto; width:50%;">
      <div class="w3-section">
        <label class="w3-xlarge"><i class="fas fa-user"></i> Username:</label>
        <span id="userEmpty" class="w3-text-red"><?=$userEmpty?></span>
        <input type="text" id="username" class="w3-input w3-border w3-round-large" name="username" value="<?=$username?>" />
      </div>

      <div class="w3-section">
        <label class="w3-xlarge"><i class="fa fa-key"></i> Password:</label>
        <span id="pswEmpty" class="w3-text-red"><?=$pswEmpty?></span>
        <input type="password" id="password" class="w3-input w3-border w3-round-large" name="password" value="<?=$password?>" />
      </div>
      
      <div class="w3-section">
        <label class="w3-xlarge"><i class="fa fa-check"></i> Conferma password:</label>
        <span id="chkPswEmpty" class="w3-text-red"><?=$pswChkEmpty?></span>
        <input type="password" id="chkPsw" class="w3-input w3-border w3-round-large" name="pswchk" />
      </div>

      <div class="w3-section w3-center">
        <button type="submit" class="w3-button w3-theme w3-round-large">
          <i class="fas fa-user-plus"></i> Registra account
        </button>
      </div>
    </form>
<?php
require 'required/footer.php';
?>