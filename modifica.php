﻿<?php
$editingPage = htmlspecialchars($_GET['page']); // Rimane da gestire il caso in cui non sia settato
// Va sanificato per bene
$pageTitle = "Modifica di \"$editingPage\"";
$topBarActiveLink = "Modifica";
$editable = true; // Qui non sono riferiti alla pagina modifica.php, ma alla pagina che si sta modificando
$hasCrono = true;

require 'required/files.php';

// Use this inside queries. $conn = connect() (to database) is defined in leftSideBar.php
$escapedEditingPage = mysqli_real_escape_string($conn, $editingPage);
$descrizEmpty = "";

if (isset($_SESSION['username'])) :
	$user = $_SESSION['username'];
  
  if ($_SERVER['REQUEST_METHOD'] == "POST") { // User clicked on "Salva le modifiche"
    if (empty(trim($_POST['descriz'])))
      $descrizEmpty = "* Campo obbligatorio";
    else { // User provided a description for the edit, so it can be saved
      $newPageContent = $_POST['newPageContent']; // Retrieve the new page content with changes made by the user
			// Retrieve the latest version id of this page
      $query = "SELECT max(id) AS verId FROM versione WHERE pagina = '$escapedEditingPage';";
      
			if ( ! ( $result = mysqli_query($conn, $query) ) )
	      handleError("DB query error: " . mysqli_error($conn));
      if ( ! ( $row = mysqli_fetch_assoc($result) ) )
	    	handleError("No match found for page $editingPage"); // Should never happens

      $verId = intval($row['verId']) + 1; // This is the correct id for the new version
      // Always escape variables to be inserted into DB queries
			$descr = mysqli_real_escape_string($conn, $_POST['descriz']);
			$dataMod = date("Y-m-d"); // Current date in YYYY-MM-DD format
      $oraMod = date("H:i:s"); // Current time in HH:MM:SS format
      // Always escape variables to be inserted into DB queries
			$newPageContent = mysqli_real_escape_string($conn, $newPageContent);
			/*$query = "START TRANSACTION; ";
			$query .= "INSERT INTO modifica (utente, pagina, dataModifica, oraModifica, descrizione)
							  							 VALUES ('$user', '$editingPage', '$dataMod', '$oraMod', '$descr'); ";
			$query .= "INSERT INTO versione (id, pagina, contenuto)
														   VALUES ($verId, '$editingPage', '$newPageContent'); ";
			$query .= "COMMIT;";*/
      $query = "INSERT INTO versione (id, pagina, contenuto)
														  VALUES ($verId, '$editingPage', '$newPageContent');";
      if ( ! mysqli_query($conn, $query) )
        handleError("DB query error: " . mysqli_error($conn));

			$query = "INSERT INTO modifica (utente, pagina, dataModifica, oraModifica, descrizione)
							  							VALUES ('$user', '$escapedEditingPage', '$dataMod', '$oraMod', '$descr');";
			if ( ! mysqli_query($conn, $query) )
        handleError("DB query error: " . mysqli_error($conn));
      /* If the script reaches this point, then the edit was successfully
         saved. Set session variable used in snackbar.php to show a confirm
         message ("Modifica salvata") and redirect to that page. */
      $_SESSION['notification'] = "La modifica è stata salvata";
      header("Location:leggi.php?page=$editingPage");
      exit();
    }
  } // End of if ($_SERVER['REQUEST_METHOD'] == "POST")
  /* The script goes here if the user hasn't sent POST data yet (by clicking on "Salva"),
     or if he can't submit (description is empty) */
	$query = "SELECT contenuto
						FROM versione
						WHERE pagina = '$escapedEditingPage'
						      AND id = (SELECT max(id) FROM versione WHERE pagina = '$escapedEditingPage');";
	if ( ! ( $result = mysqli_query($conn, $query) ) )
		handleError("DB query error: " . mysqli_error($conn));

  if ( ! ( $row = mysqli_fetch_assoc($result) ) )
    noSuchPage($editingPage);

  $oldPageContent = stripslashes($row['contenuto']);
  if (isset($_POST['newPageContent'])) // If user sent POST data but left description empty...
    $oldPageContent = $_POST['newPageContent']; // ... update textarea content with user changes
?>
    <h1>Modifica il contenuto della pagina «<?=$editingPage?>»</h1>
    <p>
			Usa il tag HTML <code>&lt;h2&gt;</code> per i titoli delle sezioni,
      <code>&lt;p&gt;</code> per i paragrafi, <code>&lt;code&gt;</code> per il codice in linea e
      <code>&lt;pre&gt;</code> per blocchi di codice. Non usare altri tag.
		</p>
    <form method="post" onsubmit="return checkEdit()">
      <label class="w3-large">
        <i class="fas fa-wrench"></i> Area di modifica
        <span id="wrongTxtArea" class="w3-text-red"></span>
      </label>
      <textarea id="txtArea" name="newPageContent" class="w3-margin-bottom"><?=$oldPageContent?></textarea>
      <label class="w3-large">
        <i class="fas fa-pencil-alt"></i> Descrivi brevemente cosa hai fatto:
        <span id="descrizEmpty" class="w3-text-red"><?=$descrizEmpty?></span>
      </label>
      <input id="descriz" type="text" name="descriz" class="w3-input w3-border w3-round-large w3-margin-bottom" />
      <div class="w3-center w3-margin-bottom">
        <button type="submit" class="w3-button w3-theme w3-round-large">
          <i class="far fa-save"></i> Salva le modifiche
        </button>
        <a href="leggi.php?page=<?=$editingPage?>" class="w3-button w3-red undo w3-round-large">
          <i class="fas fa-times"></i> Annulla
        </a>
      </div>
    </form>

		<!-- Modal to confirm exit without saving -->
		<div id="modal" class="w3-modal w3-animate-opacity">
			<div class="w3-modal-content w3-animate-top">
				<div class="w3-container w3-theme">
					<h3><i class="fas fa-info-circle"></i> Messaggio da <em>Cppedia</em>...</h3>
				</div>
				<div class="w3-container">
					<p>
						Se abbandoni la pagina, tutte le modifiche che hai effettuato andranno perse: confermi?
					</p>
					<div class="w3-center w3-margin-bottom">
						<button type="button" class="w3-button w3-theme w3-round-large"
                    onclick="document.getElementById('modal').style.display='none';window.addEventListener('beforeunload', myfun);">
							<i class="fas fa-check"></i> Rimani nella pagina
						</button>
						<a id="abbandona" class="w3-button w3-red undo w3-round-large">
							<i class="fas fa-times"></i> Abbandona la pagina
						</a>
					</div>
				</div>
			</div>
		</div>
    
    <script>
      window.addEventListener('beforeunload', myfun);
      function myfun(event) { event.preventDefault(); }
      var aEls = document.getElementsByTagName('a'); // Get all <a> elements in the page...
      for (var i = 0; i < aEls.length; i++)
        aEls[i].addEventListener('click', confirmExit); // ... and add them an event handler
      // Remove the event handler from the active link ("Modifica"):
      // if you click on it, you're not leaving the page.
      document.getElementsByClassName('active')[0].removeEventListener('click', confirmExit);
    </script>
<?php
else : // $_SESSION['username'] not set => User not logged in => User can't edit
?>
    <h1>Modifica il contenuto della pagina «<?=$editingPage?>»</h1>
    <p>Occorre essere utenti registrati per modificare una pagina di <em>Cppedia</em>.</p>
    <p class="w3-center">
      <a href="login.php?returnTo=modifica.php?page=<?=$editingPage?>" class="w3-button w3-theme w3-round-large">
        <i class="fas fa-sign-in-alt"></i> Accedi
      </a>
      oppure
      <a href="registra.php?returnTo=modifica.php?page=<?=$editingPage?>" class="w3-button w3-theme w3-round-large">
        <i class="fas fa-user-plus"></i> Crea un account
      </a>
    </p>
<?php
endif; // isset(username)

require 'required/snackbar.php';
require 'required/footer.php';
?>