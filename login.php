﻿<?php
$pageTitle = 'Entra';
$editable = false;
$hasCrono = false;
$topBarActiveLink = "Entra";

$prevPage = empty($_GET['returnTo']) ? "leggi.php?page=Home" : htmlspecialchars($_GET['returnTo']);

require 'required/files.php';

if (isset($_SESSION['username'])) {
  echo "    <h1>Entra</h1>\n";
  echo "    <p>Hai già effettuato l'accesso con l'utenza " . $_SESSION['username'] .
       ": non puoi accedere di nuovo.</p>\n";
  require 'required/footer.php';
  exit();
}

$username = ""; // Used to fill the value of <input name="username">
$userEmpty = $pswEmpty = "";

if ($_SERVER['REQUEST_METHOD'] == "POST") { // User submitted data by clicking on "Accedi"
  if (isset($_POST['username']))
    $username = trim($_POST['username']);
  if (empty($username))
    $userEmpty = "* Scrivi il tuo username";
  if (empty($_POST['password'])) // empty($var) returns true if $var is not set, i.e. it does not exist
    $pswEmpty = "* Inserisci la tua password";

  // If both fields are filled, check if their values are correct.
  if (!$userEmpty && !$pswEmpty) {
    $query = "SELECT username, password FROM utente WHERE username = '$username';";
  
    // $conn = connect() (to database) is defined in leftSideBar.php
    if ( ! ($result = mysqli_query($conn, $query) ) )
      handleError("DB query error: " . mysqli_error($conn));

    if ( ! mysqli_num_rows($result) ) // No match found for that username
      $userEmpty = "* Username errato";
    else {
      $psw = mysqli_fetch_assoc($result)['password'];
      if (md5($_POST['password']) != $psw)
        $pswEmpty = "* Password errata";
    }
    // If fields values are correct, the user can enter
    if (!$userEmpty && !$pswEmpty) {
      $_SESSION['username'] = $username;
      $_SESSION['notification'] = "Login effettuato"; // Used in snackbar.php which will be included by $prevPage
      header("Location:$prevPage");
      exit(); // Just for
    }
  } // End of if (both fields are filled)
} // End of if (method == POST)
// The script reaches this point if the user has not submitted any data yet, or if data is incorrect
?>
    <h1>Entra</h1>
    <p>
      Scrivi il tuo username e la tua password nei campi sottostanti, quindi premi su "Accedi" per entrare.
    </p>
    <form method="post" onsubmit="return checkLogin()" style="margin-left:auto; margin-right:auto; width:50%;">
      <div class="w3-section">
        <label class="w3-xlarge"><i class="fas fa-user"></i> Username:</label>
        <span id="userEmpty" class="w3-text-red"><?=$userEmpty?></span>
        <input type="text" id="username" class="w3-input w3-border w3-round-large" name="username" value="<?=$username?>" />
      </div>

      <div class="w3-section">
        <label class="w3-xlarge"><i class="fa fa-key"></i> Password:</label>
        <span id="pswEmpty" class="w3-text-red"><?=$pswEmpty?></span>
        <input type="password" id="password" class="w3-input w3-border w3-round-large" name="password" value="<?=$password?>" />
      </div>

      <div class="w3-section w3-center">
        <button type="submit" class="w3-button w3-theme w3-round-large">
          <i class="fas fa-sign-in-alt"></i> Accedi
        </button>
      </div>
    </form>
<?php
require 'required/footer.php';
?>