<?php
$pageTitle = "Cronologia";
$topBarActiveLink = "Cronologia";
$editable = true;
$hasCrono = true;

$page = empty($_GET['page']) ? "" : $_GET['page'];
$startDate = empty($_GET['startDate']) ? "" : $_GET['startDate'];
$endDate = empty($_GET['endDate']) ? "" : $_GET['endDate'];

require 'required/files.php';
?>
    <h1>Cronologia</h1>
    <p>Visualizza la cronologia di una pagina di <em>Cppedia</em>.</p>
    <form class="w3-section">
      <label>Pagina:</label>
      <input type="text" class="w3-margin-bottom" name="page" value="<?=$page?>" /><br />
      <label>Visualizza la cronologia dal giorno</label>
      <input type="date" name="startDate" min="2019-04-01" max="<?=date("Y-m-d")?>" value="<?=$startDate?>" />
      <label>al giorno</label>
      <input type="date" class="w3-margin-bottom" name="endDate" min="2019-04-01" max="<?=date("Y-m-d")?>" value="<?=$endDate?>" /><br />
      <button type="submit" class="w3-button w3-theme w3-round-large">
        <i class="fa fa-search"></i> Cerca
      </button>
    </form>
<?php
if ( ! empty($page) ) { // Query the DB only if the user has chosen a page to look for
  $query = "SELECT utente, dataModifica, oraModifica, descrizione
            FROM modifica
            WHERE pagina = '$page'";

  $dataFilter = "";
  if ( !empty($startDate) && empty($endDate) )
    $dataFilter = " AND dataModifica >= '$startDate'";
  else if ( empty($startDate) && !empty($endDate) )
    $dataFilter = " AND dataModifica <= '$endDate'";
  else if ( !empty($startDate) && !empty($endDate) )
    $dataFilter = " AND dataModifica BETWEEN '$startDate' AND '$endDate'";
  // Show edits from the most recent (in the first table row) to the least recent
  $dataFilter .= " ORDER BY dataModifica DESC, oraModifica DESC;";

  $query .= $dataFilter;

  // $conn = connect() (to database) is defined in leftSideBar.php
  if ( ! ( $result = mysqli_query($conn, $query) ) )
    handleError("DB query error: " . mysqli_error($conn));
  if ( !mysqli_num_rows($result) ) {
?>
    <p>Nessun risultato corrisponde ai criteri selezionati.</p>
<?php
  }
  else {
?>
    <table class="w3-table-all w3-margin-bottom">
      <caption class="w3-xlarge">
        Cronologia della pagina <a href="leggi.php?page=<?=$page?>"><?=$page?></a>
      </caption>
      <thead>
        <tr class="w3-theme">
          <th>Autore della modifica</th>
          <th>Data della modifica</th>
          <th>Ora della modifica</th>
          <th>Descrizione della modifica</th>
        </tr>
      </thead>
      <tbody>
<?php
    while ($row = mysqli_fetch_assoc($result)) :
      $utente = $row['utente'];
      $data = date_create_from_format("Y-m-d", $row['dataModifica']); // Create a DateTime object
      $data = date_format($data, "j/n/Y"); // Format data as dd/mm/yyyy without leading zeros
?>
        <tr>
          <td><a href="contributi.php?user=<?=$utente?>"><?=$utente?></a></td>
          <td><?=$data?></td>
          <td><?=$row['oraModifica']?></td>
          <td><?=$row['descrizione']?></td>
        </tr>
<?php
    endwhile;
?>
      </tbody>
    </table>
<?php
  }
}
require 'required/snackbar.php';
require 'required/footer.php';
?>