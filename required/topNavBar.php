<?php
// Logic to determine which links in the topNavBar are active, enabled or disabled
$aClass = "w3-bar-item w3-button w3-right"; // Base class for <a> elements inside the topNavBar
$modClass = $userClass = $logInOutClass = $cronoClass = $leggiClass = $aClass;
// Tooltips to explains why "Modifica" and "Cronologia" are disabled in special pages like login.php
$modToolTip = $cronoTooltip = "";
// Content of the first two links (from right) in topNavBar changes when the user log in or out
if (isset($_SESSION['username'])) {
  // When logged in, the first button shows his/her username and links to his/her contributions...
  $userHref = "contributi.php?user=" . urlencode($_SESSION['username']);
  $userTxt = $_SESSION['username'];
  // ... while the second one shows "Esci" and links to logout.php
  $logInOutHref = "logout.php?returnTo=" . $_SERVER['REQUEST_URI']; // The URI used to access the page which is including this one
  $logInOutIcon = "fas fa-sign-out-alt";
  $logInOutTxt = "Esci";
}
else {
  // When logged out, the first button shows "Registrati" and links to registra.php...
  $userHref = "registra.php?returnTo=" . $_SERVER['REQUEST_URI'];
  $userTxt = "Registrati";
  // ... while the second one shows "Entra" and links to login.php
  $logInOutHref = "login.php?returnTo=" . $_SERVER['REQUEST_URI'];
  $logInOutIcon = "fas fa-sign-in-alt";
  $logInOutTxt = "Entra";
}
switch ($topBarActiveLink) { // Defined in each .php page
  case "Leggi":
    $leggiClass .= " active";
    $leggiHref = "#"; // Make the link to the current page inactive
    $modHref = "modifica.php?page=" . urlencode($pageTitle); // Defined in leggi.php
    $cronoHref = "cronologia.php?page=" . urlencode($pageTitle);
    break;
  case "Modifica":
    $modClass .= " active";
    $modHref = "#"; // Make the link to the current page inactive
    $leggiHref = "leggi.php?page=" . urlencode($editingPage); // Defined in modifica.php
    // If the page is editable, it also has a chronology
    $cronoHref = "cronologia.php?page=" . urlencode($editingPage);
    break;
  case "Cronologia":
    $cronoClass .= " active";
    $cronoHref = "#"; // Make the link to the current page inactive
    $leggiHref = "leggi.php?page=" . urlencode($page); // Defined in cronologia.php
    // If the page has a cronology, it is also editable
    $modHref = "modifica.php?page=" . urlencode($page);
    break;
  case "Entra": // login.php
    $leggiClass .= " active";
    $leggiHref = "#";
    $modClass .= " disabled"; // This page is not editable
    $modHref = "#";
    $cronoClass .= " disabled"; // This page has no chronology
    $cronoHref = "#";
    $modTooltip = "Questa è una pagina speciale e non può essere modificata";
    $cronoTooltip = "Questa è una pagina speciale e non ha una cronologia";
    $logInOutClass .= " active";
    $logInOutHref = "#"; // Make the link to the current page inactive
    break;
  case "Registrati":
    $leggiClass .= " active";
    $leggiHref = "#";
    $modClass .= " disabled"; // This page is not editable
    $modHref = "#";
    $cronoClass .= " disabled"; // This page has no chronology
    $cronoHref = "#";
    $modTooltip = "Questa è una pagina speciale e non può essere modificata";
    $cronoTooltip = "Questa è una pagina speciale e non ha una cronologia";
    $userClass .= " active";
    $userHref = "#"; // Make the link to the current page inactive
    break;
  case "Contributi": case "Esci": case "Cerca":
    $leggiClass .= " active";
    $leggiHref = "#";
    $modClass .= " disabled"; // This page is not editable
    $modHref = "#";
    $cronoClass .= " disabled"; // This page has no chronology
    $cronoHref = "#";
    $modTooltip = "Questa è una pagina speciale e non può essere modificata";
    $cronoTooltip = "Questa è una pagina speciale e non ha una cronologia";
    break;
}
?>
<!-- Top navbar -->
<div class="w3-top w3-theme w3-text-white w3-card-4">
  <div class="w3-bar">
    <a href="leggi.php?page=Home" class="w3-bar-item w3-button">
      <i class="fa fa-home"></i> Cppedia
    </a>
    <a href="<?=$userHref?>" class="<?=$userClass?>"> <!-- Registrati / Username -->
      <i class="fa fa-user"></i> <?=$userTxt?>
    </a>
    <a href="<?=$logInOutHref?>" class="<?=$logInOutClass?>"> <!-- Entra / Esci -->
      <i class="<?=$logInOutIcon?>"></i> <?=$logInOutTxt?>
    </a>
    <a href="<?=$cronoHref?>" class="<?=$cronoClass?>" title="<?=$cronoTooltip?>">
      <i class="fas fa-history"></i> Cronologia
    </a>
    <a href="<?=$modHref?>" class="<?=$modClass?>" title="<?=$modTooltip?>">
      <i class="fa fa-edit"></i> Modifica
    </a>
    <a href="<?=$leggiHref?>" class="<?=$leggiClass?>">
      <i class="fab fa-readme"></i> Leggi
    </a>
    <form action="cerca.php">
      <button type="submit" class="<?=$aClass?>" style="border-right: solid #78a3ba;">
        <i class="fa fa-search"></i>
      </button>
      <input type="text" name="page" class="w3-bar-item w3-input w3-right"
             style="border: none !important; border-radius: 0;" placeholder="Cerca...">
    </form>
  </div> <!-- End of top navbar -->
</div> <!-- End of top sticky class -->
