<?php session_start(); /* Always start a session on each page */ ?>
<!DOCTYPE html>
<html lang="it">
<head>
  <meta charset="utf-8" />
  <title>Cppedia - <?=$pageTitle?></title> <!-- $pageTitle is defined in each page requiring this one -->
  <link href="css/w3.css" rel="stylesheet" /> <!-- From https://www.w3schools.com/w3css/4/w3.css -->
  <link href="css/style.css" rel="stylesheet" /> <!-- Customed CSS -->
  <link href="css/fontawesome-v5.8.1/css/all.css" rel="stylesheet" /> <!-- From https://use.fontawesome.com/releases/v5.8.1/fontawesome-free-5.8.1-web.zip -->
  <link href="images/cppediaLogo.png" rel="shortcut icon" type="image/x-icon">
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <!-- <script src="js/jquery.js"></script> --> <!-- Not used -->
  <script src="js/functions.js"></script>
</head>
<body>
