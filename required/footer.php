  </div> <!-- End of w3-card -->

  <footer class="w3-panel w3-card-4 w3-theme w3-round-large">
    <p class="w3-center">
      Il contenuto del sito è disponibile secondo la licenza CC0 e, ove possibile,
      collocato nel pubblico dominio. Tu puoi quindi liberamente copiare, modificare e
      riutilizzare, anche a fini commerciali, il materiale presente nel sito.
    </p>
    <div class="w3-center w3-section">
      <span title="Licenza Creative Commons Zero">
        <i class="fab fa-creative-commons fa-2x"></i>
        <i class="fab fa-creative-commons-zero fa-2x"></i>
      </span>
      <i class="fab fa-creative-commons-pd-alt fa-2x" title="Pubblico dominio"></i>
      <i class="fab fa-creative-commons-pd fa-2x" title="Nessun diritto riservato"></i>
      <i class="fab fa-creative-commons-share fa-2x" title="Libertà di copiare"></i>
      <i class="fab fa-creative-commons-remix fa-2x" title="Libertà di modificare"></i>
    </div>
  </footer>
</div> <!-- End of main content -->
</body>
</html>
<?php mysqli_close($conn); /* Close the connection opened in leftSideBar.php */ ?>