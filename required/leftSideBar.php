<!-- Left sidebar -->
<div class="w3-sidebar w3-bar-block w3-theme" style="width:16%;">
  <h4>Voci d'esempio</h4>
<?php
// Opening the connection here makes it available in all pages (since every page include this one),
// and indeed every page need a connection
if ( ! ( $conn = connect() ) ) {
  echo "</div>\n"; // Close the sidebar
  require 'required/mainContent.php'; // Since it has not been included yet
  handleError("DB connection error: " . mysqli_connect_error());
}

$query = "SELECT nome FROM pagina;";
if ( ! ( $result = mysqli_query($conn, $query) ) ) {
  echo "</div>\n"; // Close the sidebar
  require 'required/mainContent.php';
  handleError("DB query error: " . mysqli_error($conn));
}

if ( ! mysqli_num_rows($result) ) { // Should never happens
  echo "</div>\n"; // Close the sidebar
  require 'required/mainContent.php';
  handleError("No pages found in our database.");
}

while ($row = mysqli_fetch_assoc($result)) :
  $pag = $row['nome'];
  $activeClass = $pageTitle == $pag ? " active" : ""; // $pagetitle is defined in each page calling this one
  $aHref = $activeClass == "" ? "leggi.php?page=$pag" : "#"; // Don't link to the page we are on
?>
  <a class="w3-bar-item w3-button<?=$activeClass?>" href="<?=$aHref?>"><?=$pag?></a>
<?php
endwhile;
?>
</div> <!-- End of left sidebar -->
