<?php
// $_SESSION['notification'] is set in other scripts (registra, login, modifica and logout).
// If there is a notification for the user, show it in the snackbar. 
if ( isset($_SESSION['notification']) ) {
?>
    <div id="snackbar"><?=$_SESSION['notification']?></div>
    <script>showSnackbar();</script>
<?php
  unset($_SESSION['notification']); // Show the message only once
}
?>