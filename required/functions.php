<?php
function connect() {
  $DB_servername = "localhost";
  $DB_username = "root";
  $DB_password = "mysql";
  $DB_dbname = "cppedia";
  return mysqli_connect($DB_servername, $DB_username, $DB_password, $DB_dbname);
}

function updateLogfile($txt) {
  $filename = "log.txt";
  $logFile = fopen($filename, "a");
  fwrite($logFile, date("l j F Y, G:i:s") . ' ' . $txt . "\n");
  fclose($logFile);
}

// Called whenever an error (DB connection, query syntax...) occurs
function handleError($errMsg) {
  updateLogfile($errMsg);
  require 'required/errore.php';
  exit();
}

// Called in leggi.php and modifica.php if the value of the URL parameter 'page' does not match
// any existing page
function noSuchPage($noSuchPage) {
  // $noSuchPage is used in noSuchPage.php
  require 'required/noSuchPage.php';
  require 'required/footer.php';
  exit();
}

?>