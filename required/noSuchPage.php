    <h1>Voce inesistente</h1>
    <p>
      La voce "<?=$noSuchPage?>" non è presente su <em>Cppedia</em>
      e al momento non è possibile crearla.
    </p>
    <div class="w3-center w3-section">
      <a href="leggi.php?page=Home" class="w3-button w3-theme w3-round-large">
        <i class="fa fa-home"></i> Torna alla Home
      </a>
    </div>
