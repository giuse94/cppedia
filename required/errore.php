    <h1>Errore tecnico</h1>
    <p>
      Spiacenti, si è verificato un errore tecnico. Puoi tornare alla Home Page oppure, se sei curioso,
      visualizzare i dettagli dell'errore.
    </p>
    <div class="w3-section w3-center">
      <a href="leggi.php?page=Home" class="w3-button w3-theme w3-round-large">
        <i class="fa fa-home"></i> Torna alla Home
      </a>
      <button onclick="showHideErrorInfoPanel(this)" class="w3-button w3-theme w3-round-large">
        <i class="fas fa-plus"></i> Mostra dettagli
      </button>
    </div>
    <div id="errorInfoPanel" class="w3-panel w3-theme w3-hide w3-round-large w3-section">
      <h3><i class="fas fa-info-circle"></i> Technical information</h3>
      <p><?=$errMsg?></p> <!-- $errMsg is defined in functions.php, which includes this page -->
    </div>
<?php
require 'required/footer.php';
?>