<?php
$pageTitle = "Contributi utente";
$topBarActiveLink = "Contributi";
$editable = false;
$hasCrono = false;

require 'required/files.php';

$user = empty($_GET['user']) ? "" : $_GET['user'];
$startDate = empty($_GET['startDate']) ? "" : $_GET['startDate'];
$endDate = empty($_GET['endDate']) ? "" : $_GET['endDate'];
?>
    <h1>Contributi utente</h1>
    <p>Visualizza i contributi degli utenti di <em>Cppedia</em>.</p>
    <form class="w3-section">
      <label>Nome utente:</label>
      <input type="text" class="w3-margin-bottom" name="user" value="<?=$user?>" /><br />
      <label>Visualizza i contributi dal giorno</label>
      <input type="date" name="startDate" min="2019-04-01" max="<?=date("Y-m-d")?>" value="<?=$startDate?>" />
      <label>al giorno</label>
      <input type="date" class="w3-margin-bottom" name="endDate" min="2019-04-01" max="<?=date("Y-m-d")?>" value="<?=$endDate?>" /><br />
      <button type="submit" class="w3-button w3-theme w3-round-large">
        <i class="fa fa-search"></i> Cerca
      </button>
    </form>
<?php
if ( !empty($user) ) {
  $query = "SELECT pagina, dataModifica, oraModifica, descrizione
            FROM modifica
            WHERE utente = '$user'";

  $dataFilter = "";
  if ( !empty($startDate) && empty($endDate) )
    $dataFilter = " AND dataModifica >= '$startDate'";
  else if ( empty($startDate) && !empty($endDate) )
    $dataFilter = " AND dataModifica <= '$endDate'";
  else if ( !empty($startDate) && !empty($endDate) )
    $dataFilter = " AND dataModifica BETWEEN '$startDate' AND '$endDate'";
  // Mostra le modifiche dalla più recente (nella prima riga della tabella) alla meno recente
  $dataFilter .= " ORDER BY dataModifica DESC, oraModifica DESC;";

  $query .= $dataFilter;

  // $conn = connect() (to database) is defined in leftSideBar.php
  if ( ! ($result = mysqli_query($conn, $query) ) )
    handleError("DB query error: " . mysqli_error($conn));
  if ( !mysqli_num_rows($result) ) {
?>
    <p>Nessun risultato corrisponde ai criteri selezionati.</p>
<?php
  }
  else {
?>
    <table class="w3-table-all w3-margin-bottom">
      <caption class="w3-xlarge">Contributi di <?=$user?></caption>
      <thead>
        <tr class="w3-theme">
          <th>Pagina modificata</th>
          <th>Descrizione della modifica</th>
          <th>Data della modifica</th>
          <th>Ora della modifica</th>
        </tr>
      </thead>
      <tbody>
<?php
    while ($row = mysqli_fetch_assoc($result)) :
      $pg = $row['pagina'];
      $data = date_create_from_format("Y-m-d", $row['dataModifica']); // Create a DateTime object 
      $data = date_format($data, "j/n/Y"); // Format data as dd/mm/yyyy without leading zeros
?>
        <tr>
          <td><a href="leggi.php?page=<?=$pg?>"><?=$pg?></a></td>
          <td><?=$row['descrizione']?></td>
          <td><?=$data?></td>
          <td><?=$row['oraModifica']?></td>
        </tr>
<?php
    endwhile;
?>
      </tbody>
    </table>
<?php
  }
}
require 'required/snackbar.php';
require 'required/footer.php';
?>