﻿DROP DATABASE IF EXISTS cppedia; -- Removes all tables and data from the database
CREATE DATABASE cppedia;
USE cppedia; -- Use "cppedia" as the default database for subsequent statements

-- ---------------------------
-- Table structure for `utente`
-- ---------------------------
DROP TABLE IF EXISTS `utente`; -- Removes the table definition and all table data
CREATE TABLE `utente` (
  `username` varchar(20) NOT NULL,
  `password` char(32) NOT NULL,
  `dataRegistrazione` date NOT NULL,
  `oraRegistrazione` time NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ---------------------------
-- Table structure for `pagina`
-- ---------------------------
DROP TABLE IF EXISTS `pagina`;
CREATE TABLE `pagina` (
  `nome` varchar(200) NOT NULL,
  `modificabile` tinyint(1) NOT NULL,
  PRIMARY KEY (`nome`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- -----------------------------
-- Table structure for `modifica`
-- -----------------------------
DROP TABLE IF EXISTS `modifica`;
CREATE TABLE `modifica` (
  `utente` varchar(20) NOT NULL,
  `pagina` varchar(200) NOT NULL,
  `dataModifica` date NOT NULL,
  `oraModifica` time NOT NULL,
  `descrizione` varchar(200) NOT NULL,
  PRIMARY KEY (`utente`,`pagina`,`dataModifica`,`oraModifica`),
  FOREIGN KEY (`utente`) REFERENCES `utente` (`username`),
  FOREIGN KEY (`pagina`) REFERENCES `pagina` (`nome`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- -----------------------------
-- Table structure for `versione`
-- -----------------------------
DROP TABLE IF EXISTS `versione`;
CREATE TABLE `versione` (
  `id` int(11) NOT NULL,
  `pagina` varchar(200) NOT NULL,
  `contenuto` text NOT NULL,
  PRIMARY KEY (`id`,`pagina`),
  FOREIGN KEY (`pagina`) REFERENCES `pagina` (`nome`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ---------------------------------------------------------------------------------------
-- Create sample user and pages to set up an initial and consistent state of the database.
-- Given this structure, the website is fully usable by its users.
-- ---------------------------------------------------------------------------------------
BEGIN;
INSERT INTO `utente` (username, password, dataRegistrazione, oraRegistrazione) VALUES
                     ('user1', MD5('abc'), '2019-04-01', '15:22:34');

INSERT INTO `pagina` (nome, modificabile) VALUES
                     ('Home', 1),
                     ('Operatori', 1),
                     ('Puntatori', 1),
                     ('Strutture', 1),
                     ('Vettori', 1);

INSERT INTO `modifica` (utente, pagina, dataModifica, oraModifica, descrizione) VALUES
                       ('user1', 'Home', '2019-04-01', '15:41:10', 'Creata nuova pagina'),
                       ('user1', 'Operatori', '2019-04-02', '09:02:18', 'Creata nuova pagina'),
                       ('user1', 'Puntatori', '2019-04-03', '22:41:24', 'Creata nuova pagina'),
                       ('user1', 'Strutture', '2019-04-04', '18:41:05', 'Creata nuova pagina'),
                       ('user1', 'Vettori', '2019-04-05', '10:41:16', 'Creata nuova pagina');

INSERT INTO `versione` (id, pagina, contenuto) VALUES
                       (1, 'Home', '<h1>Homepage di <em>Cppedia</em></h1>\n<p>Benvenuto su <em>Cppedia</em>.</p>'),
                       (1, 'Operatori', '<h1>Operatori</h1>\n<p>Elenco degli operatori del C++.</p>'),
                       (1, 'Puntatori', '<h1>Puntatori</h1>\n<p>Un puntatore è una variabile che contiene un indirizzo di memoria.</p>'),
                       (1, 'Strutture', '<h1>Strutture</h1>\n<p>Una struttura permette di raggruppare variabili di tipo diverso.</p>'),
                       (1, 'Vettori', '<h1>Vettori</h1>\n<p>Un vettore permette di raggruppare variabili dello stesso tipo.</p>');
COMMIT;
