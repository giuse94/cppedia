<?php
$editable = true;
/* Always editable pages in test version. Query will be:
$query = "SELECT modificabile FROM pagina where nome = '$pageTitle';";
*/
$hasCrono = true;
$pageTitle = isset($_GET['page']) ? $_GET['page'] : "Home"; // Should sanitize $_GET
$topBarActiveLink = "Leggi";

require 'required/files.php';

$query = "SELECT contenuto FROM versione
          WHERE pagina = '$pageTitle' 
                AND id = (SELECT max(id) FROM versione WHERE pagina = '$pageTitle');";

// $conn = connect() (to database) is defined in leftSideBar.php
if ( ! ( $result = mysqli_query($conn, $query) ) )
  handleError("DB query error: " . mysqli_error($conn));

if ( ! ( $row = mysqli_fetch_assoc($result) ) )
  noSuchPage($pageTitle);

$pageContent = $row['contenuto'];

echo $pageContent; // Every line should start with 4 spaces
echo "\n"; // Add an empty new line; should force it to appear at the end of $pageContent

require 'required/snackbar.php';
require 'required/footer.php';
?>